#ifndef CRIT_H
#define CRIT_H

#include <criterion/criterion.h>
#include <stdlib.h>
#include "fixtures.h"
#include "list.h"

void cr_assert_list(struct list *list, void **addrs, size_t size);

#endif // CRIT_H
