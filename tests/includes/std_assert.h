#ifndef STD_SRT_H
#define STD_SRT_H

#include <assert.h>
#include <stdlib.h>
#include "fixtures.h"
#include "list.h"

void assert_list(struct list *list, void **addrs, size_t size);

#endif // STD_SRT_H
