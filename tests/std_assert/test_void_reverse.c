#include "std_assert.h"

int test_list_void_reverse_empty_list(void)
{
    list_void_reverse(NULL);
    return 1;
}

int test_list_void_reverse_single_node_list(void)
{
    struct list *list = single_node();
    list_void_reverse(list);
    void *addrs[] = {(void *)0};

    assert_list(list, addrs, 1);

    list_clear(list);
    return 1;
}

int test_list_void_reverse_even_nodes_list(void)
{
    struct list *list = multiple_nodes();
    list = list_append(list, (void *)42);

    list_void_reverse(list);

    struct list *head = list->prev->prev->prev;
    void *addrs[] = {(void *)42, (void *)2, (void *)1, (void *)0};
    assert_list(head, addrs, 4);

    list_clear(head);
    return 1;
}

int test_list_void_reverse_odd_nodes_list(void)
{
    struct list *list = multiple_nodes();
    list = list_append(list_append(list, (void *)42), (void *)69);

    list_void_reverse(list);

    struct list *head = list->prev->prev->prev->prev;
    void *addrs[] = {(void *)69, (void *)42, (void *)2, (void *)1, (void *)0};
    assert_list(head, addrs, 5);

    list_clear(head);
    return 1;
}
