#include "std_assert.h"

int test_list_preprend_empty_list(void)
{
    struct list *list = list_prepend(NULL, (void *)42);
    void *addrs[] = {(void *)42};

    assert_list(list, addrs, 1);

    list_clear(list);
    return 1;
}

int test_list_preprend_single_node_list(void)
{
    struct list *tail = single_node();
    struct list *list = list_prepend(tail, (void *)42);
    void *addrs[] = {(void *)42, (void *)0};

    assert_list(list, addrs, 2);

    list_clear(list);
    return 1;
}
