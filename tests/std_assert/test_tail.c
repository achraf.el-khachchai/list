#include "std_assert.h"

struct list *list_tail(struct list *list);

int test_list_tail_empty_list(void)
{
    assert(!list_tail(NULL));
    return 1;
}

int test_list_tail_single_node_list(void)
{
    struct list *list = single_node();

    assert(list_tail(list) == list);

    list_clear(list);
    return 1;
}

int test_list_tail_multiple_nodes_list(void)
{
    struct list *list = multiple_nodes();

    assert(list_tail(list) == list->next->next);

    list_clear(list);
    return 1;
}
