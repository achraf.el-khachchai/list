#include "std_assert.h"

int test_list_insert_at_0_on_empty_list(void)
{
    struct list *list = list_insert(NULL, (void *)42, 0);
    void *addrs[] = {(void *)42};

    assert_list(list, addrs, 1);

    list_clear(list);
    return 1;
}

int test_list_insert_at_1_on_empty_list(void)
{
    assert(!list_insert(NULL, (void *)42, 1));
    return 1;
}

int test_list_insert_at_0_on_single_node_list(void)
{
    struct list *tail = single_node();
    struct list *list = list_insert(tail, (void *)42, 0);
    void *addrs[] = {(void *)42, (void *)0};

    assert_list(list, addrs, 2);

    list_clear(list);
    return 1;
}

int test_list_insert_at_1_on_single_node_list(void)
{
    struct list *head = single_node();
    struct list *list = list_insert(head, (void *)42, 1);
    void *addrs[] = {(void *)0, (void *)42};

    assert_list(list, addrs, 2);

    list_clear(list);
    return 1;
}

int test_list_insert_at_2_on_single_node_list(void)
{
    struct list *head = single_node();

    assert(!list_insert(head, (void *)42, 2));

    list_clear(head);
    return 1;
}

int test_list_insert_at_0_on_multiple_nodes_list(void)
{
    struct list *tail = multiple_nodes();
    struct list *list = list_insert(tail, (void *)42, 0);
    void *addrs[] = {(void *)42, (void *)0, (void *)1, (void *)2};

    assert_list(list, addrs, 4);

    list_clear(list);
    return 1;
}

int test_list_insert_at_1_on_multiple_nodes_list(void)
{
    struct list *tail = multiple_nodes();
    struct list *list = list_insert(tail, (void *)42, 1);
    void *addrs[] = {(void *)0, (void *)42, (void *)1, (void *)2};

    assert_list(list, addrs, 4);

    list_clear(list);
    return 1;
}

int test_list_insert_at_2_on_multiple_nodes_list(void)
{
    struct list *tail = multiple_nodes();
    struct list *list = list_insert(tail, (void *)42, 2);
    void *addrs[] = {(void *)0, (void *)1, (void *)42, (void *)2};

    assert_list(list, addrs, 4);

    list_clear(list);
    return 1;
}

int test_list_insert_at_3_on_multiple_nodes_list(void)
{
    struct list *tail = multiple_nodes();
    struct list *list = list_insert(tail, (void *)42, 3);
    void *addrs[] = {(void *)0, (void *)1, (void *)2, (void *)42};

    assert_list(list, addrs, 4);

    list_clear(list);
    return 1;
}

int test_list_insert_at_4_on_multiple_nodes_list(void)
{
    struct list *tail = multiple_nodes();

    assert(!list_insert(tail, (void *)42, 4));

    list_clear(tail);
    return 1;
}
