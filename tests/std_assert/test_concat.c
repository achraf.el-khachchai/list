#include "std_assert.h"

int test_list_concat_empty_list_to_emplty_list(void)
{
    struct list *a = NULL, *b = NULL;

    list_concat(a, b);

    assert(!a);
    assert(!b);

    return 1;
}

int test_list_concat_empty_list_to_list(void)
{
    struct list *a = NULL, *b = multiple_nodes();

    list_concat(a, b);

    assert(!a);
    void *addrs[] = {(void *)0, (void *)1, (void *)2};
    assert_list(b, addrs, 3);

    list_clear(b);
    return 1;
}

int test_list_concat_list_to_empty_list(void)
{
    struct list *a = multiple_nodes(), *b = NULL;

    list_concat(a, b);

    void *addrs[] = {(void *)0, (void *)1, (void *)2};
    assert_list(a, addrs, 3);
    assert(!b);

    list_clear(a);
    return 1;
}

int test_list_concat_list_to_single_node_list(void)
{
    struct list *a = multiple_nodes(), *b = single_node();

    list_concat(a, b);

    void *addrs[] = {(void *)0, (void *)1, (void *)2, (void *)0};
    assert_list(a, addrs, 4);
    assert_list(b, NULL, 0);

    list_clear(a);
    list_clear(b);
    return 1;
}

int test_list_concat_list_to_multiple_nodes_list(void)
{
    struct list *a = multiple_nodes(), *b = multiple_nodes();

    list_concat(a, b);

    void *addrs[] = {(void *)0, (void *)1, (void *)2, (void *)0, (void *)1, (void *)2};
    assert_list(a, addrs, 6);
    assert_list(b, NULL, 0);

    list_clear(a);
    list_clear(b);
    return 1;
}
