#include "std_assert.h"

int test_list_size_empty_list(void)
{
    assert(list_size(NULL) == 0);
    return 1;
}

int test_list_size_single_node_list(void)
{
    struct list *list = single_node();
    void *addrs[] = {(void *)0};
    assert_list(list, addrs, 1);

    assert(list_size(list) == 1);

    assert_list(list, addrs, 1);
    list_clear(list);
    return 1;
}

int test_list_size_multiple_nodes_list(void)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)0, (void *)1, (void *)2};
    assert_list(list, addrs, 3);

    assert(list_size(list) == 3);

    assert_list(list, addrs, 3);
    list_clear(list);
    return 1;
}
