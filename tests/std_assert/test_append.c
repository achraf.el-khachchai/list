#include "std_assert.h"

int test_list_append_empty_list(void)
{
    struct list *list = list_append(NULL, (void *)42);
    void *addrs[] = {(void *)42};

    assert_list(list, addrs, 1);

    list_clear(list);
    return 1;
}

int test_list_append_single_node_list(void)
{
    struct list *head = single_node();
    struct list *list = list_append(head, (void *)42);
    void *addrs[] = {(void *)0, (void *)42};

    assert_list(list, addrs, 2);

    list_clear(list);
    return 1;
}

int test_list_append_multiple_nodes_list(void)
{
    struct list *head = multiple_nodes();
    struct list *list = list_append(head, (void *)42);
    void *addrs[] = {(void *)0, (void *)1, (void *)2, (void *)42};

    assert_list(list, addrs, 4);

    list_clear(list);
    return 1;
}
