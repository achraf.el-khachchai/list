#include "std_assert.h"

int test_list_find_empty_list(void)
{
    assert(list_find(NULL, (void *)0) == -1);
    return 1;
}

int test_list_find_single_node_list(void)
{
    struct list *list = single_node();

    assert(list_find(list, (void *)0) == 0);
    assert(list_find(list, (void *)1) == -1);

    list_clear(list);
    return 1;
}

int test_list_find_multiple_nodes_list(void)
{
    struct list *list = multiple_nodes();

    assert(list_find(list, (void *)0) == 0);
    assert(list_find(list, (void *)1) == 1);
    assert(list_find(list, (void *)2) == 2);
    assert(list_find(list, (void *)42) == -1);

    list_clear(list);
    return 1;
}
