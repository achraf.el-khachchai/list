#include "std_assert.h"

int test_list_clear_empty_list(void)
{
    list_clear(NULL);
    return 1;
}

int test_list_clear_single_node_list(void)
{
    list_clear(single_node());
    return 1;
}

int test_list_clear_multiple_nodes_list(void)
{
    list_clear(multiple_nodes());
    return 1;
}
