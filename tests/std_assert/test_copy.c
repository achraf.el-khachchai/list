#include "std_assert.h"

int test_list_copy_empty_list(void)
{
    assert(!list_copy(NULL));
    return 1;
}

int test_list_copy_single_node_list(void)
{
    struct list *list = single_node();
    void *addrs[] = {(void *)0};

    struct list *copy = list_copy(list);

    assert(copy != list);
    assert_list(copy, addrs, 1);
    assert_list(list, addrs, 1);

    list_clear(copy);
    list_clear(list);
    return 1;
}

int test_list_copy_multiple_nodes_list(void)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)0, (void *)1, (void *)2};

    struct list *copy = list_copy(list);

    assert(copy != list);
    assert_list(copy, addrs, 3);
    assert_list(list, addrs, 3);

    list_clear(copy);
    list_clear(list);
    return 1;
}
