#include "std_assert.h"

void assert_list(struct list *list, void **addrs, size_t size)
{
    assert(list);
    assert(!list->prev);

    for (struct list *iter = list; iter && size--; iter = iter->next)
    {
        assert(iter->addr == *addrs++);
        if (size)
            assert(iter->next->prev == iter);
        else
            assert(!iter->next);
    }
}

int test_single_node_list(void)
{
    struct list *list = single_node();
    void *addrs[] = {(void *)0};

    assert_list(list, addrs, 1);

    list_clear(list);
    return 1;
}

int test_multiple_nodes_list(void)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)0, (void *)1, (void *)2};

    assert_list(list, addrs, 3);

    list_clear(list);
    return 1;
}
