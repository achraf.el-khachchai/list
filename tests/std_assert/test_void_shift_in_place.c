#include "std_assert.h"

int test_list_void_shift_in_place_empty_list(void)
{
    list_void_shift_in_place(NULL, 0);
    list_void_shift_in_place(NULL, 1);
    list_void_shift_in_place(NULL, -1);
    return 1;
}

int test_list_void_shift_in_place_single_node_list(void)
{
    struct list *list = single_node();
    void *addrs[] = {(void *)0};

    list_void_shift_in_place(list, 0);
    assert_list(list, addrs, 1);

    list_void_shift_in_place(list, 1);
    assert_list(list, addrs, 1);

    list_void_shift_in_place(list, -1);
    assert_list(list, addrs, 1);

    list_clear(list);
    return 1;
}

int test_list_void_shift_in_place_multiple_nodes_list_by_0(void)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)0, (void *)1, (void *)2};

    list_void_shift_in_place(list, 0);
    assert_list(list, addrs, 3);

    list_clear(list);
    return 1;
}

int test_list_void_shift_in_place_multiple_nodes_list_by_1(void)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)2, (void *)0, (void *)1};

    list_void_shift_in_place(list, 1);
    assert_list(list, addrs, 3);

    list_clear(list);
    return 1;
}

int test_list_void_shift_in_place_multiple_nodes_list_by_2(void)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)1, (void *)2, (void *)0};

    list_void_shift_in_place(list, 2);
    assert_list(list, addrs, 3);

    list_clear(list);
    return 1;
}

int test_list_void_shift_in_place_multiple_nodes_list_by_3(void)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)0, (void *)1, (void *)2};

    list_void_shift_in_place(list, 3);
    assert_list(list, addrs, 3);

    list_clear(list);
    return 1;
}

int test_list_void_shift_in_place_multiple_nodes_list_by_7(void)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)2, (void *)0, (void *)1};

    list_void_shift_in_place(list, 7);
    assert_list(list, addrs, 3);

    list_clear(list);
    return 1;
}

int test_list_void_shift_in_place_multiple_nodes_list_by_minus_1(void)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)1, (void *)2, (void *)0};

    list_void_shift_in_place(list, -1);
    assert_list(list, addrs, 3);

    list_clear(list);
    return 1;
}

int test_list_void_shift_in_place_multiple_nodes_list_by_minus_2(void)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)2, (void *)0, (void *)1};

    list_void_shift_in_place(list, -2);
    assert_list(list, addrs, 3);

    list_clear(list);
    return 1;
}

int test_list_void_shift_in_place_multiple_nodes_list_by_minus_3(void)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)0, (void *)1, (void *)2};

    list_void_shift_in_place(list, -3);
    assert_list(list, addrs, 3);

    list_clear(list);
    return 1;
}

int test_list_void_shift_in_place_multiple_nodes_list_by_minus_7(void)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)1, (void *)2, (void *)0};

    list_void_shift_in_place(list, -7);
    assert_list(list, addrs, 3);

    list_clear(list);
    return 1;
}
