#include "std_assert.h"

int test_list_init(void)
{
    struct list *list = list_init();
    void *addrs[] = {NULL};

    assert_list(list, addrs, 1);

    list_clear(list);
    return 1;
}
