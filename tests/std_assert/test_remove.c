#include "std_assert.h"

int test_list_remove_empty_list(void)
{
    assert(!list_remove(NULL, 0));
    return 1;
}

int test_list_remove_at_0_on_single_node_list(void)
{
    struct list *list = list_remove(single_node(), 0);
    void *addrs[] = {(void *)0};

    assert_list(list, addrs, 1);

    list_clear(list);
    return 1;
}

int test_list_remove_at_1_on_single_node_list(void)
{
    struct list *list = single_node();
    void *addrs[] = {(void *)0};

    assert(!list_remove(list, 1));
    assert_list(list, addrs, 1);

    list_clear(list);
    return 1;
}

int test_list_remove_at_0_on_multiple_nodes_list(void)
{
    struct list *list = multiple_nodes();
    struct list *next = list->next;
    list = list_remove(list, 0);
    void *list_addrs[] = {(void *)0};
    void *next_addrs[] = {(void *)1, (void *)2};

    assert_list(list, list_addrs, 1);
    assert_list(next, next_addrs, 2);

    list_clear(list);
    list_clear(next);
    return 1;
}

int test_list_remove_at_1_on_multiple_nodes_list(void)
{
    struct list *list = multiple_nodes();
    struct list *node = list_remove(list, 1);
    void *node_addrs[] = {(void *)1};
    void *list_addrs[] = {(void *)0, (void *)2};

    assert_list(node, node_addrs, 1);
    assert_list(list, list_addrs, 2);

    list_clear(node);
    list_clear(list);
    return 1;
}

int test_list_remove_at_2_on_multiple_nodes_list(void)
{
    struct list *list = multiple_nodes();
    struct list *node = list_remove(list, 2);
    void *node_addrs[] = {(void *)2};
    void *list_addrs[] = {(void *)0, (void *)1};

    assert_list(node, node_addrs, 1);
    assert_list(list, list_addrs, 2);

    list_clear(node);
    list_clear(list);
    return 1;
}

int test_list_remove_at_3_on_multiple_nodes_list(void)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)0, (void *)1, (void *)2};

    assert(!list_remove(list, 3));
    assert_list(list, addrs, 3);

    list_clear(list);
    return 1;
}
