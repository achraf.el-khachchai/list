#include "std_assert.h"

int test_list_split_at_empty_list(void)
{
    assert(!list_split_at(NULL, 0));
    assert(!list_split_at(NULL, 1));
    return 1;
}

int test_list_split_at_0_onsingle_node_list(void)
{
    struct list *list = single_node();

    struct list *split = list_split_at(list, 0);

    assert(split == list);
    void *addrs[] = {(void *)0};
    assert_list(list, addrs, 1);
    assert_list(split, addrs, 1);

    list_clear(list);
    return 1;
}

int test_list_split_at_1_onsingle_node_list(void)
{
    struct list *list = single_node();

    assert(!list_split_at(list, 1));

    list_clear(list);
    return 1;
}

int test_list_split_at_0_on_multiple_nodes_list(void)
{
    struct list *list = multiple_nodes();

    struct list *split = list_split_at(list, 0);

    assert(split == list);
    void *addrs[] = {(void *)0, (void *)1, (void *)2};
    assert_list(list, addrs, 3);
    assert_list(split, addrs, 3);

    list_clear(list);
    return 1;
}

int test_list_split_at_1_on_multiple_nodes_list(void)
{
    struct list *list = multiple_nodes();

    struct list *split = list_split_at(list, 1);

    assert(split != list);
    void *l_addrs[] = {(void *)0};
    void *s_addrs[] = {(void *)1, (void *)2};
    assert_list(list, l_addrs, 1);
    assert_list(split, s_addrs, 2);

    list_clear(list);
    list_clear(split);
    return 1;
}

int test_list_split_at_2_on_multiple_nodes_list(void)
{
    struct list *list = multiple_nodes();

    struct list *split = list_split_at(list, 2);

    assert(split != list);
    void *l_addrs[] = {(void *)0, (void *)1};
    void *s_addrs[] = {(void *)2};
    assert_list(list, l_addrs, 2);
    assert_list(split, s_addrs, 1);

    list_clear(list);
    list_clear(split);
    return 1;
}

int test_list_split_at_3_on_multiple_nodes_list(void)
{
    struct list *list = multiple_nodes();

    assert(!list_split_at(list, 3));

    list_clear(list);
    return 1;
}
