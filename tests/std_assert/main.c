#include <stdio.h>
#include "std_assert.h"

int test_single_node_list(void);
int test_multiple_nodes_list(void);

int test_list_clear_empty_list(void);
int test_list_clear_single_node_list(void);
int test_list_clear_multiple_nodes_list(void);

int test_list_init(void);

int test_list_preprend_empty_list(void);
int test_list_preprend_single_node_list(void);

int test_list_append_empty_list(void);
int test_list_append_single_node_list(void);
int test_list_append_multiple_nodes_list(void);

int test_list_size_empty_list(void);
int test_list_size_single_node_list(void);
int test_list_size_multiple_nodes_list(void);

int test_list_get_empty_list(void);
int test_list_get_single_node_list(void);
int test_list_get_multiple_nodes_list(void);

int test_list_insert_at_0_on_empty_list(void);
int test_list_insert_at_1_on_empty_list(void);

int test_list_insert_at_0_on_single_node_list(void);
int test_list_insert_at_1_on_single_node_list(void);
int test_list_insert_at_2_on_single_node_list(void);

int test_list_insert_at_0_on_multiple_nodes_list(void);
int test_list_insert_at_1_on_multiple_nodes_list(void);
int test_list_insert_at_2_on_multiple_nodes_list(void);
int test_list_insert_at_3_on_multiple_nodes_list(void);
int test_list_insert_at_4_on_multiple_nodes_list(void);

int test_list_remove_empty_list(void);

int test_list_remove_at_0_on_single_node_list(void);
int test_list_remove_at_1_on_single_node_list(void);

int test_list_remove_at_0_on_multiple_nodes_list(void);
int test_list_remove_at_1_on_multiple_nodes_list(void);
int test_list_remove_at_2_on_multiple_nodes_list(void);
int test_list_remove_at_3_on_multiple_nodes_list(void);

int test_list_find_empty_list(void);
int test_list_find_single_node_list(void);
int test_list_find_multiple_nodes_list(void);

int test_list_tail_empty_list(void);
int test_list_tail_single_node_list(void);
int test_list_tail_multiple_nodes_list(void);

int test_list_reverse_empty_list(void);
int test_list_reverse_single_node_list(void);
int test_list_reverse_even_nodes_list(void);
int test_list_reverse_odd_nodes_list(void);

int test_list_void_reverse_empty_list(void);
int test_list_void_reverse_single_node_list(void);
int test_list_void_reverse_even_nodes_list(void);
int test_list_void_reverse_odd_nodes_list(void);

int test_list_void_reverse_in_place_empty_list(void);
int test_list_void_reverse_in_place_single_node_list(void);
int test_list_void_reverse_in_place_even_nodes_list(void);
int test_list_void_reverse_in_place_odd_nodes_list(void);

int test_list_split_at_empty_list(void);

int test_list_split_at_0_onsingle_node_list(void);
int test_list_split_at_1_onsingle_node_list(void);

int test_list_split_at_0_on_multiple_nodes_list(void);
int test_list_split_at_1_on_multiple_nodes_list(void);
int test_list_split_at_2_on_multiple_nodes_list(void);
int test_list_split_at_3_on_multiple_nodes_list(void);

int test_list_concat_empty_list_to_emplty_list(void);
int test_list_concat_empty_list_to_list(void);
int test_list_concat_list_to_empty_list(void);
int test_list_concat_list_to_single_node_list(void);
int test_list_concat_list_to_multiple_nodes_list(void);

int test_list_shift_empty_list(void);
int test_list_shift_single_node_list(void);
int test_list_shift_multiple_nodes_list_by_0(void);
int test_list_shift_multiple_nodes_list_by_1(void);
int test_list_shift_multiple_nodes_list_by_2(void);
int test_list_shift_multiple_nodes_list_by_3(void);
int test_list_shift_multiple_nodes_list_by_7(void);
int test_list_shift_multiple_nodes_list_by_minus_1(void);
int test_list_shift_multiple_nodes_list_by_minus_2(void);
int test_list_shift_multiple_nodes_list_by_minus_3(void);
int test_list_shift_multiple_nodes_list_by_minus_7(void);

int test_list_void_shift_empty_list(void);
int test_list_void_shift_single_node_list(void);
int test_list_void_shift_multiple_nodes_list_by_0(void);
int test_list_void_shift_multiple_nodes_list_by_1(void);
int test_list_void_shift_multiple_nodes_list_by_2(void);
int test_list_void_shift_multiple_nodes_list_by_3(void);
int test_list_void_shift_multiple_nodes_list_by_7(void);
int test_list_void_shift_multiple_nodes_list_by_minus_1(void);
int test_list_void_shift_multiple_nodes_list_by_minus_2(void);
int test_list_void_shift_multiple_nodes_list_by_minus_3(void);
int test_list_void_shift_multiple_nodes_list_by_minus_7(void);

int test_list_void_shift_in_place_empty_list(void);
int test_list_void_shift_in_place_single_node_list(void);
int test_list_void_shift_in_place_multiple_nodes_list_by_0(void);
int test_list_void_shift_in_place_multiple_nodes_list_by_1(void);
int test_list_void_shift_in_place_multiple_nodes_list_by_2(void);
int test_list_void_shift_in_place_multiple_nodes_list_by_3(void);
int test_list_void_shift_in_place_multiple_nodes_list_by_7(void);
int test_list_void_shift_in_place_multiple_nodes_list_by_minus_1(void);
int test_list_void_shift_in_place_multiple_nodes_list_by_minus_2(void);
int test_list_void_shift_in_place_multiple_nodes_list_by_minus_3(void);
int test_list_void_shift_in_place_multiple_nodes_list_by_minus_7(void);

int test_list_copy_empty_list(void);
int test_list_copy_single_node_list(void);
int test_list_copy_multiple_nodes_list(void);

int test_list_remove_eq_empty_list(void);

int test_list_remove_eq_at_0_on_single_node_list(void);
int test_list_remove_eq_at_1_on_single_node_list(void);

int test_list_remove_eq_at_0_on_multiple_nodes_list(void);
int test_list_remove_eq_at_1_on_multiple_nodes_list(void);
int test_list_remove_eq_at_2_on_multiple_nodes_list(void);
int test_list_remove_eq_at_3_on_multiple_nodes_list(void);

int main(void)
{
    int count = 0;

    count += test_single_node_list();
    count += test_multiple_nodes_list();

    count += test_list_clear_empty_list();
    count += test_list_clear_single_node_list();
    count += test_list_clear_multiple_nodes_list();

    count += test_list_init();

    count += test_list_preprend_empty_list();
    count += test_list_preprend_single_node_list();

    count += test_list_append_empty_list();
    count += test_list_append_single_node_list();
    count += test_list_append_multiple_nodes_list();

    count += test_list_size_empty_list();
    count += test_list_size_single_node_list();
    count += test_list_size_multiple_nodes_list();

    count += test_list_get_empty_list();
    count += test_list_get_single_node_list();
    count += test_list_get_multiple_nodes_list();

    count += test_list_insert_at_0_on_empty_list();
    count += test_list_insert_at_1_on_empty_list();

    count += test_list_insert_at_0_on_single_node_list();
    count += test_list_insert_at_1_on_single_node_list();
    count += test_list_insert_at_2_on_single_node_list();

    count += test_list_insert_at_0_on_multiple_nodes_list();
    count += test_list_insert_at_1_on_multiple_nodes_list();
    count += test_list_insert_at_2_on_multiple_nodes_list();
    count += test_list_insert_at_3_on_multiple_nodes_list();
    count += test_list_insert_at_4_on_multiple_nodes_list();

    count += test_list_remove_empty_list();

    count += test_list_remove_at_0_on_single_node_list();
    count += test_list_remove_at_1_on_single_node_list();

    count += test_list_remove_at_0_on_multiple_nodes_list();
    count += test_list_remove_at_1_on_multiple_nodes_list();
    count += test_list_remove_at_2_on_multiple_nodes_list();
    count += test_list_remove_at_3_on_multiple_nodes_list();

    count += test_list_find_empty_list();
    count += test_list_find_single_node_list();
    count += test_list_find_multiple_nodes_list();

    count += test_list_tail_empty_list();
    count += test_list_tail_single_node_list();
    count += test_list_tail_multiple_nodes_list();

    count += test_list_reverse_empty_list();
    count += test_list_reverse_single_node_list();
    count += test_list_reverse_even_nodes_list();
    count += test_list_reverse_odd_nodes_list();

    count += test_list_void_reverse_empty_list();
    count += test_list_void_reverse_single_node_list();
    count += test_list_void_reverse_even_nodes_list();
    count += test_list_void_reverse_odd_nodes_list();

    count += test_list_void_reverse_in_place_empty_list();
    count += test_list_void_reverse_in_place_single_node_list();
    count += test_list_void_reverse_in_place_even_nodes_list();
    count += test_list_void_reverse_in_place_odd_nodes_list();

    count += test_list_split_at_empty_list();

    count += test_list_split_at_0_onsingle_node_list();
    count += test_list_split_at_1_onsingle_node_list();

    count += test_list_split_at_0_on_multiple_nodes_list();
    count += test_list_split_at_1_on_multiple_nodes_list();
    count += test_list_split_at_2_on_multiple_nodes_list();
    count += test_list_split_at_3_on_multiple_nodes_list();

    count += test_list_concat_empty_list_to_emplty_list();
    count += test_list_concat_empty_list_to_list();
    count += test_list_concat_list_to_empty_list();
    count += test_list_concat_list_to_single_node_list();
    count += test_list_concat_list_to_multiple_nodes_list();

    count += test_list_shift_empty_list();
    count += test_list_shift_single_node_list();
    count += test_list_shift_multiple_nodes_list_by_0();
    count += test_list_shift_multiple_nodes_list_by_1();
    count += test_list_shift_multiple_nodes_list_by_2();
    count += test_list_shift_multiple_nodes_list_by_3();
    count += test_list_shift_multiple_nodes_list_by_7();
    count += test_list_shift_multiple_nodes_list_by_minus_1();
    count += test_list_shift_multiple_nodes_list_by_minus_2();
    count += test_list_shift_multiple_nodes_list_by_minus_3();
    count += test_list_shift_multiple_nodes_list_by_minus_7();

    count += test_list_void_shift_empty_list();
    count += test_list_void_shift_single_node_list();
    count += test_list_void_shift_multiple_nodes_list_by_0();
    count += test_list_void_shift_multiple_nodes_list_by_1();
    count += test_list_void_shift_multiple_nodes_list_by_2();
    count += test_list_void_shift_multiple_nodes_list_by_3();
    count += test_list_void_shift_multiple_nodes_list_by_7();
    count += test_list_void_shift_multiple_nodes_list_by_minus_1();
    count += test_list_void_shift_multiple_nodes_list_by_minus_2();
    count += test_list_void_shift_multiple_nodes_list_by_minus_3();
    count += test_list_void_shift_multiple_nodes_list_by_minus_7();

    count += test_list_void_shift_in_place_empty_list();
    count += test_list_void_shift_in_place_single_node_list();
    count += test_list_void_shift_in_place_multiple_nodes_list_by_0();
    count += test_list_void_shift_in_place_multiple_nodes_list_by_1();
    count += test_list_void_shift_in_place_multiple_nodes_list_by_2();
    count += test_list_void_shift_in_place_multiple_nodes_list_by_3();
    count += test_list_void_shift_in_place_multiple_nodes_list_by_7();
    count += test_list_void_shift_in_place_multiple_nodes_list_by_minus_1();
    count += test_list_void_shift_in_place_multiple_nodes_list_by_minus_2();
    count += test_list_void_shift_in_place_multiple_nodes_list_by_minus_3();
    count += test_list_void_shift_in_place_multiple_nodes_list_by_minus_7();

    count += test_list_copy_empty_list();
    count += test_list_copy_single_node_list();
    count += test_list_copy_multiple_nodes_list();

    count += test_list_remove_eq_empty_list();

    count += test_list_remove_eq_at_0_on_single_node_list();
    count += test_list_remove_eq_at_1_on_single_node_list();

    count += test_list_remove_eq_at_0_on_multiple_nodes_list();
    count += test_list_remove_eq_at_1_on_multiple_nodes_list();
    count += test_list_remove_eq_at_2_on_multiple_nodes_list();
    count += test_list_remove_eq_at_3_on_multiple_nodes_list();

    printf("Passed %d tests.\n", count);
    return 0;
}
