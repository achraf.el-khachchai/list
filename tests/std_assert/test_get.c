#include "std_assert.h"

int test_list_get_empty_list(void)
{
    assert(!list_get(NULL, 0));
    assert(!list_get(NULL, 1));
    return 1;
}

int test_list_get_single_node_list(void)
{
    struct list *list = single_node();

    assert(list_get(list, 0) == list);
    assert(!list_get(list, 1));

    list_clear(list);
    return 1;
}

int test_list_get_multiple_nodes_list(void)
{
    struct list *list = multiple_nodes();

    assert(list_get(list, 0) == list);
    assert(list_get(list, 1) == list->next);
    assert(list_get(list, 2) == list->next->next);
    assert(!list_get(list, 3));

    list_clear(list);
    return 1;
}
