#include "std_assert.h"

int test_list_reverse_empty_list(void)
{
    list_reverse(NULL);
    return 1;
}

int test_list_reverse_single_node_list(void)
{
    struct list *list = single_node();
    list_reverse(list);
    void *addrs[] = {(void *)0};

    assert_list(list, addrs, 1);

    list_clear(list);
    return 1;
}

int test_list_reverse_even_nodes_list(void)
{
    struct list *list = multiple_nodes();
    list = list_append(list, (void *)42);

    list = list_reverse(list);

    void *addrs[] = {(void *)42, (void *)2, (void *)1, (void *)0};
    assert_list(list, addrs, 4);

    list_clear(list);
    return 1;
}

int test_list_reverse_odd_nodes_list(void)
{
    struct list *list = multiple_nodes();
    list = list_append(list_append(list, (void *)42), (void *)69);

    list = list_reverse(list);

    void *addrs[] = {(void *)69, (void *)42, (void *)2, (void *)1, (void *)0};
    assert_list(list, addrs, 5);

    list_clear(list);
    return 1;
}
