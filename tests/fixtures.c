#include "fixtures.h"
#include "list.h"

struct list *single_node()
{
    return list_prepend(NULL, (void *)0);
}

struct list *multiple_nodes()
{
    void *zero = (void *)0, *one = (void *)1, *two = (void *)2;
    return list_prepend(list_prepend(list_prepend(NULL, two), one), zero);
}
