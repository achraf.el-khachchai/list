#include "criterion.h"

Test(list_reverse, empty_list)
{
    list_reverse(NULL);
}

Test(list_reverse, single_node_list)
{
    struct list *list = single_node();
    list_reverse(list);
    void *addrs[] = {(void *)0};

    cr_assert_list(list, addrs, 1);

    list_clear(list);
}

Test(list_reverse, even_nodes_list)
{
    struct list *list = multiple_nodes();
    list = list_append(list, (void *)42);

    list = list_reverse(list);

    void *addrs[] = {(void *)42, (void *)2, (void *)1, (void *)0};
    cr_assert_list(list, addrs, 4);

    list_clear(list);
}

Test(list_reverse, odd_nodes_list)
{
    struct list *list = multiple_nodes();
    list = list_append(list_append(list, (void *)42), (void *)69);

    list = list_reverse(list);

    void *addrs[] = {(void *)69, (void *)42, (void *)2, (void *)1, (void *)0};
    cr_assert_list(list, addrs, 5);

    list_clear(list);
}