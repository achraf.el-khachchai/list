#include "criterion.h"

Test(list_get, empty_list)
{
    cr_assert(!list_get(NULL, 0));
    cr_assert(!list_get(NULL, 1));
}

Test(list_get, single_node_list)
{
    struct list *list = single_node();

    cr_assert(list_get(list, 0) == list);
    cr_assert(!list_get(list, 1));

    list_clear(list);
}

Test(list_get, multiple_nodes_list)
{
    struct list *list = multiple_nodes();

    cr_assert(list_get(list, 0) == list);
    cr_assert(list_get(list, 1) == list->next);
    cr_assert(list_get(list, 2) == list->next->next);
    cr_assert(!list_get(list, 3));

    list_clear(list);
}
