#include "criterion.h"

Test(list_size, empty_list)
{
    cr_assert(list_size(NULL) == 0);
}

Test(list_size, single_node_list)
{
    struct list *list = single_node();

    cr_assert(list_size(list) == 1);

    list_clear(list);
}

Test(list_size, multiple_nodes_list)
{
    struct list *list = multiple_nodes();

    cr_assert(list_size(list) == 3);

    list_clear(list);
}
