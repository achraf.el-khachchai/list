#include "criterion.h"

Test(list_find, empty_list)
{
    cr_assert(list_find(NULL, (void *)0) == -1);
}

Test(list_find, single_node_list)
{
    struct list *list = single_node();

    cr_assert(list_find(list, (void *)0) == 0);
    cr_assert(list_find(list, (void *)1) == -1);

    list_clear(list);
}

Test(list_find, multiple_nodes_list)
{
    struct list *list = multiple_nodes();

    cr_assert(list_find(list, (void *)0) == 0);
    cr_assert(list_find(list, (void *)1) == 1);
    cr_assert(list_find(list, (void *)2) == 2);
    cr_assert(list_find(list, (void *)42) == -1);

    list_clear(list);
}