#include "criterion.h"

struct list *list_tail(struct list *list);

Test(list_tail, empty_list)
{
    cr_assert(!list_tail(NULL));
}

Test(list_tail, single_node_list)
{
    struct list *list = single_node();

    cr_assert(list_tail(list) == list);

    list_clear(list);
}

Test(list_tail, multiple_nodes_list)
{
    struct list *list = multiple_nodes();

    cr_assert(list_tail(list) == list->next->next);

    list_clear(list);
}
