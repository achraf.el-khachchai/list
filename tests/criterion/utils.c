#include "criterion.h"

void cr_assert_list(struct list *list, void **addrs, size_t size)
{
    cr_assert(list);
    cr_assert(!list->prev);

    for (struct list *iter = list; iter && size--; iter = iter->next)
    {
        cr_assert(iter->addr == *addrs++);
        if (size)
            cr_assert(iter->next->prev == iter);
        else
            cr_assert(!iter->next);
    }
}

Test(cr_assert_list, single_node_list)
{
    struct list *list = single_node();
    void *addrs[] = {(void *)0};

    cr_assert_list(list, addrs, 1);

    list_clear(list);
}

Test(cr_assert_list, multiple_nodes_list)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)0, (void *)1, (void *)2};

    cr_assert_list(list, addrs, 3);

    list_clear(list);
}
