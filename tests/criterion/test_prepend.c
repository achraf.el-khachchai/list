#include "criterion.h"

Test(list_preprend, empty_list)
{
    struct list *list = list_prepend(NULL, (void *)42);
    void *addrs[] = {(void *)42};

    cr_assert_list(list, addrs, 1);

    list_clear(list);
}

Test(list_preprend, single_node_list)
{
    struct list *tail = single_node();
    struct list *list = list_prepend(tail, (void *)42);
    void *addrs[] = {(void *)42, (void *)0};

    cr_assert_list(list, addrs, 2);

    list_clear(list);
}
