#include "criterion.h"

Test(list_void_shift, empty_list)
{
    list_void_shift(NULL, 0);
    list_void_shift(NULL, 1);
    list_void_shift(NULL, -1);
}

Test(list_void_shift, single_node_list)
{
    struct list *list = single_node();
    void *addrs[] = {(void *)0};

    list_void_shift(list, 0);
    cr_assert_list(list, addrs, 1);

    list_void_shift(list, 1);
    cr_assert_list(list, addrs, 1);

    list_void_shift(list, -1);
    cr_assert_list(list, addrs, 1);

    list_clear(list);
}

Test(list_void_shift, multiple_nodes_list_by_0)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)0, (void *)1, (void *)2};

    list_void_shift(list, 0);
    cr_assert_list(list, addrs, 3);

    list_clear(list);
}

Test(list_void_shift, multiple_nodes_list_by_1)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)2, (void *)0, (void *)1};

    list_void_shift(list, 1);

    struct list *head = list->prev;
    cr_assert_list(head, addrs, 3);

    list_clear(head);
}

Test(list_void_shift, multiple_nodes_list_by_2)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)1, (void *)2, (void *)0};

    list_void_shift(list, 2);

    struct list *head = list->prev->prev;
    cr_assert_list(head, addrs, 3);

    list_clear(head);
}

Test(list_void_shift, multiple_nodes_list_by_3)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)0, (void *)1, (void *)2};

    list_void_shift(list, 3);
    cr_assert_list(list, addrs, 3);

    list_clear(list);
}

Test(list_void_shift, multiple_nodes_list_by_7)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)2, (void *)0, (void *)1};

    list_void_shift(list, 7);

    struct list *head = list->prev;
    cr_assert_list(head, addrs, 3);

    list_clear(head);
}

Test(list_void_shift, multiple_nodes_list_by_minus_1)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)1, (void *)2, (void *)0};

    list_void_shift(list, -1);

    struct list *head = list->prev->prev;
    cr_assert_list(head, addrs, 3);

    list_clear(head);
}

Test(list_void_shift, multiple_nodes_list_by_minus_2)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)2, (void *)0, (void *)1};

    list_void_shift(list, -2);

    struct list *head = list->prev;
    cr_assert_list(head, addrs, 3);

    list_clear(head);
}

Test(list_void_shift, multiple_nodes_list_by_minus_3)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)0, (void *)1, (void *)2};

    list_void_shift(list, -3);
    cr_assert_list(list, addrs, 3);

    list_clear(list);
}

Test(list_void_shift, multiple_nodes_list_by_minus_7)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)1, (void *)2, (void *)0};

    list_void_shift(list, -7);

    struct list *head = list->prev->prev;
    cr_assert_list(head, addrs, 3);

    list_clear(head);
}
