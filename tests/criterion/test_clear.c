#include "criterion.h"

Test(list_clear, empty_list)
{
    list_clear(NULL);
}

Test(list_clear, single_node_list)
{
    list_clear(single_node());
}

Test(list_clear, multiple_nodes_list)
{
    list_clear(multiple_nodes());
}
