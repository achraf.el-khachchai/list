#include "criterion.h"

Test(list_append, empty_list)
{
    struct list *list = list_append(NULL, (void *)42);
    void *addrs[] = {(void *)42};

    cr_assert_list(list, addrs, 1);

    list_clear(list);
}

Test(list_append, single_node_list)
{
    struct list *head = single_node();
    struct list *list = list_append(head, (void *)42);
    void *addrs[] = {(void *)0, (void *)42};

    cr_assert_list(list, addrs, 2);

    list_clear(list);
}

Test(list_append, multiple_nodes_list)
{
    struct list *head = multiple_nodes();
    struct list *list = list_append(head, (void *)42);
    void *addrs[] = {(void *)0, (void *)1, (void *)2, (void *)42};

    cr_assert_list(list, addrs, 4);

    list_clear(list);
}
