#include "criterion.h"

Test(list_copy, empty_list)
{
    cr_assert(!list_copy(NULL));
}

Test(list_copy, single_node_list)
{
    struct list *list = single_node();
    void *addrs[] = {(void *)0};

    struct list *copy = list_copy(list);

    cr_assert(copy != list);
    cr_assert_list(copy, addrs, 1);
    cr_assert_list(list, addrs, 1);

    list_clear(copy);
    list_clear(list);
}

Test(list_copy, multiple_nodes_list)
{
    struct list *list = multiple_nodes();
    void *addrs[] = {(void *)0, (void *)1, (void *)2};

    struct list *copy = list_copy(list);

    cr_assert(copy != list);
    cr_assert_list(copy, addrs, 3);
    cr_assert_list(list, addrs, 3);

    list_clear(copy);
    list_clear(list);
}
