#include "criterion.h"

Test(list_concat, empty_list_to_emplty_list)
{
    struct list *a = NULL, *b = NULL;

    list_concat(a, b);

    cr_assert(!a);
    cr_assert(!b);
}

Test(list_concat, empty_list_to_list)
{
    struct list *a = NULL, *b = multiple_nodes();

    list_concat(a, b);

    cr_assert(!a);
    void *addrs[] = {(void *)0, (void *)1, (void *)2};
    cr_assert_list(b, addrs, 3);

    list_clear(b);
}

Test(list_concat, list_to_empty_list)
{
    struct list *a = multiple_nodes(), *b = NULL;

    list_concat(a, b);

    void *addrs[] = {(void *)0, (void *)1, (void *)2};
    cr_assert_list(a, addrs, 3);
    cr_assert(!b);

    list_clear(a);
}

Test(list_concat, list_to_single_node_list)
{
    struct list *a = multiple_nodes(), *b = single_node();

    list_concat(a, b);

    void *addrs[] = {(void *)0, (void *)1, (void *)2, (void *)0};
    cr_assert_list(a, addrs, 4);
    cr_assert_list(b, NULL, 0);

    list_clear(a);
    list_clear(b);
}

Test(list_concat, list_to_multiple_nodes_list)
{
    struct list *a = multiple_nodes(), *b = multiple_nodes();

    list_concat(a, b);

    void *addrs[] = {(void *)0, (void *)1, (void *)2, (void *)0, (void *)1, (void *)2};
    cr_assert_list(a, addrs, 6);
    cr_assert_list(b, NULL, 0);

    list_clear(a);
    list_clear(b);
}
