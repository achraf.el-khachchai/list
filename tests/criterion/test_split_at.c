#include "criterion.h"

Test(list_split_at, empty_list)
{
    cr_assert(!list_split_at(NULL, 0));
    cr_assert(!list_split_at(NULL, 1));
}

Test(list_split_at, _0_onsingle_node_list)
{
    struct list *list = single_node();

    struct list *split = list_split_at(list, 0);

    cr_assert(split == list);
    void *addrs[] = {(void *)0};
    cr_assert_list(list, addrs, 1); // Actually, list should be `NULL`.
    cr_assert_list(split, addrs, 1);

    list_clear(list);
}

Test(list_split_at, _1_onsingle_node_list)
{
    struct list *list = single_node();

    cr_assert(!list_split_at(list, 1));

    list_clear(list);
}

Test(list_split_at, _0_on_multiple_nodes_list)
{
    struct list *list = multiple_nodes();

    struct list *split = list_split_at(list, 0);

    cr_assert(split == list);
    void *addrs[] = {(void *)0, (void *)1, (void *)2};
    cr_assert_list(list, addrs, 3);
    cr_assert_list(split, addrs, 3);

    list_clear(list);
}

Test(list_split_at, _1_on_multiple_nodes_list)
{
    struct list *list = multiple_nodes();

    struct list *split = list_split_at(list, 1);

    cr_assert(split != list);
    void *l_addrs[] = {(void *)0};
    void *s_addrs[] = {(void *)1, (void *)2};
    cr_assert_list(list, l_addrs, 1);
    cr_assert_list(split, s_addrs, 2);

    list_clear(list);
    list_clear(split);
}

Test(list_split_at, _2_on_multiple_nodes_list)
{
    struct list *list = multiple_nodes();

    struct list *split = list_split_at(list, 2);

    cr_assert(split != list);
    void *l_addrs[] = {(void *)0, (void *)1};
    void *s_addrs[] = {(void *)2};
    cr_assert_list(list, l_addrs, 2);
    cr_assert_list(split, s_addrs, 1);

    list_clear(list);
    list_clear(split);
}

Test(list_split_at, _3_on_multiple_nodes_list)
{
    struct list *list = multiple_nodes();

    cr_assert(!list_split_at(list, 3));

    list_clear(list);
}
