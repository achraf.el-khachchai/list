#include "criterion.h"

Test(list_insert, at_0_on_empty_list)
{
    struct list *list = list_insert(NULL, (void *)42, 0);
    void *addrs[] = {(void *)42};

    cr_assert_list(list, addrs, 1);

    list_clear(list);
}

Test(list_insert, at_1_on_empty_list)
{
    cr_assert(!list_insert(NULL, (void *)42, 1));
}

Test(list_insert, at_0_on_single_node_list)
{
    struct list *tail = single_node();
    struct list *list = list_insert(tail, (void *)42, 0);
    void *addrs[] = {(void *)42, (void *)0};

    cr_assert_list(list, addrs, 2);

    list_clear(list);
}

Test(list_insert, at_1_on_single_node_list)
{
    struct list *head = single_node();
    struct list *list = list_insert(head, (void *)42, 1);
    void *addrs[] = {(void *)0, (void *)42};

    cr_assert_list(list, addrs, 2);

    list_clear(list);
}

Test(list_insert, at_2_on_single_node_list)
{
    struct list *head = single_node();

    cr_assert(!list_insert(head, (void *)42, 2));

    list_clear(head);
}

Test(list_insert, at_0_on_multiple_nodes_list)
{
    struct list *tail = multiple_nodes();
    struct list *list = list_insert(tail, (void *)42, 0);
    void *addrs[] = {(void *)42, (void *)0, (void *)1, (void *)2};

    cr_assert_list(list, addrs, 4);

    list_clear(list);
}

Test(list_insert, at_1_on_multiple_nodes_list)
{
    struct list *tail = multiple_nodes();
    struct list *list = list_insert(tail, (void *)42, 1);
    void *addrs[] = {(void *)0, (void *)42, (void *)1, (void *)2};

    cr_assert_list(list, addrs, 4);

    list_clear(list);
}

Test(list_insert, at_2_on_multiple_nodes_list)
{
    struct list *tail = multiple_nodes();
    struct list *list = list_insert(tail, (void *)42, 2);
    void *addrs[] = {(void *)0, (void *)1, (void *)42, (void *)2};

    cr_assert_list(list, addrs, 4);

    list_clear(list);
}

Test(list_insert, at_3_on_multiple_nodes_list)
{
    struct list *tail = multiple_nodes();
    struct list *list = list_insert(tail, (void *)42, 3);
    void *addrs[] = {(void *)0, (void *)1, (void *)2, (void *)42};

    cr_assert_list(list, addrs, 4);

    list_clear(list);
}

Test(list_insert, at_4_on_multiple_nodes_list)
{
    struct list *tail = multiple_nodes();

    cr_assert(!list_insert(tail, (void *)42, 4));

    list_clear(tail);
}
