#include "criterion.h"

Test(list_void_reverse, empty_list)
{
    list_void_reverse(NULL);
}

Test(list_void_reverse, single_node_list)
{
    struct list *list = single_node();
    list_void_reverse(list);
    void *addrs[] = {(void *)0};

    cr_assert_list(list, addrs, 1);

    list_clear(list);
}

Test(list_void_reverse, even_nodes_list)
{
    struct list *list = multiple_nodes();
    list = list_append(list, (void *)42);

    list_void_reverse(list);

    struct list *head = list->prev->prev->prev;
    void *addrs[] = {(void *)42, (void *)2, (void *)1, (void *)0};
    cr_assert_list(head, addrs, 4);

    list_clear(head);
}

Test(list_void_reverse, odd_nodes_list)
{
    struct list *list = multiple_nodes();
    list = list_append(list_append(list, (void *)42), (void *)69);

    list_void_reverse(list);

    struct list *head = list->prev->prev->prev->prev;
    void *addrs[] = {(void *)69, (void *)42, (void *)2, (void *)1, (void *)0};
    cr_assert_list(head, addrs, 5);

    list_clear(head);
}