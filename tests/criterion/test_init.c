#include "criterion.h"

Test(list_init, ok)
{
    struct list *list = list_init();
    void *addrs[] = {NULL};

    cr_assert_list(list, addrs, 1);

    list_clear(list);
}
