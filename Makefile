CC = gcc
HEADERS = -I src/includes -I tests/includes
CFLAGS = -Werror -Wall -Wextra -pedantic -std=c99 -g $(HEADERS)
SRC=$(wildcard src/*.c)
OBJ=$(SRC:.c=.o)
CRIT_TEST_SRC=tests/fixtures.c $(wildcard tests/criterion/*.c)
CRIT_TEST_OBJ=$(CRIT_TEST_SRC:.c=.o)
ASSERT_TEST_SRC=tests/fixtures.c $(wildcard tests/std_assert/*.c)
ASSERT_TEST_OBJ=$(ASSERT_TEST_SRC:.c=.o)
LIBS=-lcriterion

.PHONY: all clean check grind crit_check assert_check crit_grind assert_grind

all: crit_test assert_test

check: crit_test assert_test
	./crit_test.out
	./assert_test.out

grind: crit_grind assert_grind

crit_check: crit_test
	./crit_test.out

assert_check: assert_test
	./assert_test.out

crit_grind: crit_test
	valgrind --leak-check=full --trace-children=yes ./crit_test.out

assert_grind: assert_test
	valgrind --leak-check=full ./assert_test.out

crit_test: $(OBJ) $(CRIT_TEST_OBJ)
	$(CC) $(CFLAGS) -o "$@.out" $^ $(LIBS)

$(CRIT_TEST_OBJ): $(CRIT_TEST_SRC)

assert_test: $(OBJ) $(ASSERT_TEST_OBJ)
	$(CC) $(CFLAGS) -o "$@.out" $^

$(ASSERT_TEST_OBJ): $(ASSERT_TEST_SRC)

clean:
	rm -f $(OBJ) $(CRIT_TEST_OBJ) $(ASSERT_TEST_OBJ) crit_test.out assert_test.out