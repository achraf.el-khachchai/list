#include <stdlib.h>
#include "list.h"

struct list *list_init(void)
{
    struct list *list = malloc(sizeof(struct list));

    if (list)
    {
        list->addr = NULL;
        list->next = NULL;
        list->prev = NULL;
    }

    return list;
}

static struct list *list_create(void *addr)
{
    struct list *list = list_init();

    if (list)
        list->addr = addr;

    return list;
}

struct list *list_tail(struct list *list)
{
    while (list && list->next)
        list = list->next;
    return list;
}

struct list *list_prepend(struct list *list, void *addr)
{
    struct list *head = list_create(addr);

    if (!head)
        return NULL;

    head->next = list;
    if (list)
        list->prev = head;

    return head;
}

struct list *list_append(struct list *list, void *addr)
{
    struct list *node = list_create(addr);

    if (!node)
        return NULL;

    if (list)
    {
        struct list *tail = list_tail(list);
        tail->next = node;
        node->prev = tail;
    }

    return list ? list : node;
}

size_t list_size(struct list *list)
{
    size_t size = 0;

    for (; list; list = list->next)
        size++;

    return size;
}

void list_clear(struct list *list)
{
    if (list)
        for (struct list *next = list->next; list; list = next, next = next ? next->next : NULL)
            free(list);
}

struct list *list_get(struct list *list, size_t index)
{
    if (!list)
        return NULL;

    for (size_t i = 0; list && i++ < index; list = list->next)
        ;

    return list;
}

struct list *list_insert(struct list *list, void *addr, size_t index)
{
    if (!index)
        return list_prepend(list, addr);

    if (index == list_size(list))
        return list_append(list, addr);

    struct list *next = list_get(list, index);
    if (!next)
        return NULL;

    struct list *node = list_create(addr);
    if (!node)
        return NULL;

    struct list *prev = next->prev;
    node->next = next;
    node->prev = prev;
    prev->next = node;
    next->prev = node;

    return list;
}

struct list *list_remove(struct list *list, size_t index)
{
    struct list *node = list_get(list, index);

    if (!node)
        return NULL;

    struct list *next = node->next, *prev = node->prev;
    if (next)
        next->prev = prev;
    if (prev)
        prev->next = next;

    node->next = NULL;
    node->prev = NULL;
    return node;
}

long long list_find(struct list *list, void *addr)
{

    for (long long i = 0; list; i++, list = list->next)
        if (list->addr == addr)
            return i;

    return -1;
}

struct list *list_reverse(struct list *list)
{
    struct list *tail = list_tail(list);

    if (list)
        for (struct list *next = list->next; list; list = next, next = next ? next->next : NULL)
        {
            list->next = list->prev;
            list->prev = next;
        }

    return tail;
}

void list_void_reverse(struct list *list)
{
    if (list)
        for (struct list *next = list->next; list; list = next, next = next ? next->next : NULL)
        {
            list->next = list->prev;
            list->prev = next;
        }
}

void list_void_reverse_in_place(struct list *list)
{
    for (struct list *tail = list_tail(list); list != tail && list->prev != tail; list = list->next, tail = tail->prev)
    {
        void *tmp = list->addr;
        list->addr = tail->addr;
        tail->addr = tmp;
    }
}

struct list *list_split_at(struct list *list, size_t index)
{
    if (!list || !index)
        return list;

    struct list *head = list_get(list, index);

    if (head)
    {
        head->prev->next = NULL;
        head->prev = NULL;
    }

    return head;
}

void list_concat(struct list *list1, struct list *list2)
{
    if (!list1)
        return;

    if (list2)
    {
        struct list *tail = list_tail(list1);
        struct list *head = list_create(list2->addr);

        if (head)
        {
            tail->next = head;
            head->prev = tail;
            head->next = list2->next;

            if (list2->next)
                list2->next->prev = head;

            list2->addr = NULL;
            list2->next = NULL;
        }
    }
}

static size_t list_shift_offset(struct list *list, int offset)
{
    long long size = list_size(list);
    return (size + offset % size) % size;
}

struct list *list_shift(struct list *list, int offset)
{
    if (!list || !offset)
        return list;

    size_t shifts = list_shift_offset(list, offset);
    struct list *tail = list_tail(list);

    for (size_t i = 0; i < shifts; i++)
    {
        list->prev = tail;
        tail->next = list;

        list = tail;
        tail = tail->prev;

        list->prev = NULL;
        tail->next = NULL;
    }

    return list;
}

void list_void_shift(struct list *list, int offset)
{
    if (!list || !offset)
        return;

    size_t shifts = list_shift_offset(list, offset);
    struct list *tail = list_tail(list);

    for (size_t i = 0; i < shifts; i++)
    {
        list->prev = tail;
        tail->next = list;

        list = tail;
        tail = tail->prev;

        list->prev = NULL;
        tail->next = NULL;
    }
}

void list_void_shift_in_place(struct list *list, int offset)
{
    if (!list || !offset)
        return;

    void *addr = list->addr;
    size_t shifts = list_shift_offset(list, offset);

    for (size_t i = 0; i < shifts; i++)
        for (struct list *next = list->next; next; next = next->next)
        {
            void *tmp = next->addr;
            next->addr = addr;
            addr = tmp;
        }

    list->addr = addr;
}

struct list *list_remove_eq(struct list *list, void *addr)
{
    long long index = list_find(list, addr);

    if (index == -1)
        return NULL;

    return list_remove(list, index);
}

struct list *list_copy(struct list *list)
{
    struct list *copy = NULL;

    for (list = list_tail(list); list; list = list->prev)
        copy = list_prepend(copy, list->addr);

    return copy;
}
