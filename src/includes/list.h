#ifndef LIST_H
#define LIST_H

#include <stddef.h>

struct list
{
    void *addr;
    struct list *next;
    struct list *prev;
};

/*
** Allocates a new list and returns a pointer to it.
** Returns `NULL` if the allocation fails.
*/
struct list *list_init(void);

/*
** Returns the last element of the list.
** Returns `NULL` if the list is empty.
*/
struct list *list_tail(struct list *list);

/*
** Prepends a new element to the list and returns a pointer to the new list.
** Returns `NULL` if an error occured.
*/
struct list *list_prepend(struct list *list, void *addr);

/*
** Appends a new element to the list and returns a pointer to the new list.
** Returns `NULL` if an error occured.
*/
struct list *list_append(struct list *list, void *addr);

/*
** Returns the size of the list.
*/
size_t list_size(struct list *list);

/*
** Empties the list.
*/
void list_clear(struct list *list);

/*
** Returns the element at the given index.
** Returns `NULL` if the index is out of range.
*/
struct list *list_get(struct list *list, size_t index);

/*
** Inserts a new element at the given index and returns a pointer to the new list.
** Returns `NULL` if an error occured or if index is out of range.
** If index is equal to the size of the list is valid.
*/
struct list *list_insert(struct list *list, void *addr, size_t index);

/*
** Removes the element at the given index and returns a pointer to it.
** Returns `NULL` if an error occured or if index is out of range.
*/
struct list *list_remove(struct list *list, size_t index);

/*
** Returns the index of the first element that is equal to the given address.
** Returns -1 if the element is not found.
*/
long long list_find(struct list *list, void *addr);

/*
** Reverse the list and returns the head of the reversed list.
*/
struct list *list_reverse(struct list *list);

/*
** Reverse the list.
** The list parameter that was passed now is the tail of the list.
*/
void list_void_reverse(struct list *list);

/*
** Reverse the list in place.
** The list that was passed still is the head of the list.
*/
void list_void_reverse_in_place(struct list *list);

/*
** Splits the list at the given index and returns a pointer to the new list.
** Returns `NULL` if an error occured or if index is out of range.
*/
struct list *list_split_at(struct list *list, size_t index);

/*
** Concatenates list2 to list1 and returns a pointer to the new list.
** Returns `NULL` if an error occured.
** The list2 must be emptied.
*/
void list_concat(struct list *list1, struct list *list2);

/*
** Shifts the list by the given offset and returns the head of the shifted list.
*/
struct list *list_shift(struct list *list, int offset);

/*
** Shifts the list by the given offset.
** The list parameter that was passed now is the offset-th node of the list.
*/
void list_void_shift(struct list *list, int offset);

/*
** Shifts the list in place by the given offset.
** The list that was passed still is the head of the list.
*/
void list_void_shift_in_place(struct list *list, int offset);

/*
** Removes the first element that is equal to the given address and returns a pointer it.
** Returns `NULL` if an error occured.
*/
struct list *list_remove_eq(struct list *list, void *addr);

/*
** Returns a deep copy of the list.
*/
struct list *list_copy(struct list *list);

/*
** Wikipedia bonus.
*/
unsigned int list_levenshtein(struct list *list1, struct list *list2);

#endif // LIST_H
